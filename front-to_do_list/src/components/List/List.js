import React, {Component} from 'react';
import './style.css'
import getUser from '../../resolvers/getUser'
import existUser from '../../resolvers/existUser'
import queryTaskLists from '../../services/queryTaskList';
import addChore from '../../services/addChore';
import removeTaskList from '../../services/removeTaskList';
import removeChore from '../../services/deleteChore';
import addTaskList from '../../services/addTaskList';

class List extends Component{
    constructor(){
        super();
        this.state={
            userList:[],
            newChoreName:"",
            newTaskListName:""
        }
    }

    componentDidMount(){
        if(existUser()){
            this.rechargeData()
        }else{
            localStorage.setItem('user', null)
        }
    }

    rechargeData=()=>{
        if(existUser()){
            queryTaskLists(getUser()).then((resp)=>{  
                this.setState({
                    userList:resp.data.data.queryOneTaskList,
                    newChoreName:"",
                    newTaskListName:""
                })
            })
        }else{
            console.log("Doesn´t exists")
        }
    }

    updateValue = (e)=>{
        let {name, value} = e.target;
        this.setState({
            [name]:value
        })
    }


    addChoreFunction = ( id, choreName)=>{
        if(this.state.newChoreName!==""){
            addChore(id, choreName).then(()=>{
                this.rechargeData();
            })
        }else{
            return;
        }
    }

    removeChoresFunction = (listId, choreName)=>{
        removeChore(listId, choreName).then(()=>{
            this.rechargeData();
        })
    }

    addTaskListFunction = ()=>{
        addTaskList(getUser(), this.state.newTaskListName).then(()=>{
            this.rechargeData();
        })
    }

    deleteTaskListFunction = (id)=>{
        removeTaskList(id, getUser()).then(()=>{
            this.rechargeData();
        });
    }

    showAddTaskListModal = ()=>{
        if(localStorage.getItem('user')!==null && existUser()){
            return(this.addTaskListModal())
        }else{
            return(
                <div name="segundodiv" className="container">
                    <div  className="row d-flex align-items-center justify-content-center full-screen">
                        <h3 className="title"> Select a user</h3>
                    </div>
                </div>
            )
        } 
    }

    addTaskListModal = ()=>{
        return(
            <div className="distance d-flex align-items-center justify-content-center">
                <button type="button" className="btn btn-info" data-toggle="modal" data-target="#addTaskListModal">+ Add task list</button>

                <div className="modal fade" id="addTaskListModal" tabIndex="-1" role="dialog" aria-labelledby="addTaskListModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="addTaskListModalLabel">Add a task list here</h5>
                                <button type="button"  className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="newChoreNameAdon">Task list name:</span>
                                        </div>
                                        <input type="text" name="newTaskListName" value={this.state.newTaskListName } onChange={this.updateValue} className="form-control" id="choreNameInput"  placeholder="..."/> 
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" data-dismiss="modal" className="btn btn-primary" onClick={()=>this.addTaskListFunction()} key="addTaskListModalClose">Add!</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderList = ()=>{
        if(this.state.userList!==null){
            let listFinal = this.state.userList.map((list, index)=>{
                let choresList = list.chores.map((chore, indexC)=>{
                    return(
                        <div className="input-group" key={indexC}>
                            <div className="container">
                                <div className="row">
                                    <div className="col-10">
                                        <span className="input-group-text" id="basic-addon2">
                                            <div className="input-group-prepend">
                                            
                                                <div className="input-group-text" >
                                                    {chore.choreName}
                                                </div>
                                            
                                            </div>
                                        </span>
                                    </div>
                                    <div className="col-2  justify-content-center d-flex align-items-center">
                                        <button className="btn btn-success"  onClick={()=>{this.removeChoresFunction(list._id, chore.choreName)}}>Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
                return (
                    <div className="container">
                        <div className="row">
                            <div className="col-8">
                                <div className="card" key={`list${index}`} >
                                    <button className="btn btn-dark" data-toggle="collapse" data-target={`#collapse${index}`} aria-expanded="true" aria-controls={`collapse${index}`}>
                                        <div className="card-header" id={`heading${index}`}>
                                            <h5 className="mb-1 ">
                                                {list.listName}
                                            </h5>
                                        </div>
                                    </button>
                                    <div id={`collapse${index}`} className="collapse" aria-labelledby={`heading${index}`} data-parent="#accordion">
                                        <div className="card-body">
                                            {choresList}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-1 d-flex align-items-center justify-content-center offset-1">
                                <button type="button" className="btn btn-primary" data-toggle="modal"  data-target={`#modal${index}`}>Add Chore</button>

                                <div className="modal fade" id={`modal${index}`} tabIndex="-1" role="dialog" aria-labelledby={`modalTitle${index}`} aria-hidden="true">
                                    <div className="modal-dialog modal-dialog-centered" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h5 className="modal-title" id={`modalTitle${index}`}>{list.listName}</h5>
                                                <button type="button"  className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            
                                            <div className="modal-body">
                                                <div className="form-group">
                                                    <div className="input-group mb-3">
                                                        <div className="input-group-prepend">
                                                            <span className="input-group-text" id="newChoreNameAdon">Chore name:</span>
                                                        </div>
                                                        <input type="text" name="newChoreName" value={this.state.newChoreName} onChange={this.updateValue} className="form-control" id="choreNameInput" aria-describedby="choreNameHelp" placeholder="..."/> 
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="modal-footer">
                                                <button type="button" data-dismiss="modal" className="btn btn-primary" onClick={()=>{this.addChoreFunction(list._id,this.state.newChoreName)}}>Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-1 justify-content-center">  
                                    <button className="btn btn-danger" onClick={()=>{this.deleteTaskListFunction(list._id)}}>Delete List</button>                                
                                </div>
                            </div>
                        </div>
                    </div>
                                
                )
            })
            
            return (
                <div >
                    {this.showAddTaskListModal()}
                    {listFinal}
                </div>
                );
        }else{
            return(
                <div >
                    {this.showAddTaskListModal()}
                </div>
            )
        }
    }

    render(){
        return(
            <div id="accordion">
                {this.renderList()}
            </div>
        )
    }
};

export default List;
