import React, {Component} from 'react';
import allUsers from '../../services/queryAllUsers';
import './style.css'
import '../../'
import getUser from '../../resolvers/getUser'
import List from '../../components/List/List'
import addUser from '../../services/addUser';
import removeUser from '../../services/removeUser';

class Home extends Component{
    constructor(){
        super();
        this.state={
            users:"",
            newUserName:""
        }
    }

    componentDidMount(){
        allUsers().then((res)=>{
            this.setState({
                users:res.data.data.queryAllU
            })
        }).catch((err)=>{
            console.log("Problem=>", err)
        })
    }

    updateValue = (e)=>{
        let {name, value} = e.target;
        this.setState({
            [name]:value
        })
    }

    rechargeUsers = ()=>{
        allUsers().then((res)=>{
            this.setState({
                users:res.data.data.queryAllU,
                newUserName:"",
            })
        })
    }

    selectUser = (id)=>{
        localStorage.setItem('user', id)
        this.refs.list.rechargeData();
        this.rechargeUsers();
    } 

    addUserFunction = ()=>{
        addUser(this.state.newUserName).then(()=>{
            this.rechargeUsers();
        })
    }

    removeUserFunction = (id)=>{
        removeUser(id).then(()=>{
            if(id===getUser()){
                localStorage.removeItem('user')
            };
            this.rechargeUsers();
        })
    }

    createUserModal = ()=>{
        return (
            <div>
                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#addUserModal">+ Add user</button>
                
                <div className="modal fade" id="addUserModal" tabIndex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="addUserModalLabel">Add user</h5>
                                <button type="button"  className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="newChoreNameAdon">User name:</span>
                                        </div>
                                        <input type="text" name="newUserName" value={this.state.newUserName } onChange={this.updateValue} className="form-control" id="userNameInput"  placeholder="..."/> 
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" data-dismiss="modal" className="btn btn-primary" onClick={()=>this.addUserFunction()}>Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    

    renderUsers = ()=>{
        if(this.state.users!==""){
            let usersShown = this.state.users.map((user, index)=>{
                return(
                    <div className="container" key={`user${index}`}>
                        <div className="row">
                            <div className="col-1 justify-content-center d-flex align-items-center">
                                <button onClick={()=>this.removeUserFunction(user._id)} className="btn btn-ligth">x</button>
                            </div>
                            <div className="col">
                                <div onClick={()=>this.selectUser(user._id)} key={index}>
                                    <a  className={(user._id===getUser())?"list-group-item list-group-item-action active":"list-group-item list-group-item-action"}>
                                        {user.name}
                                    </a>
                                </div> 
                            </div>
                        </div>
                    </div> 
                                        
                )
            })
        return usersShown;
        }else{
            return(
                <div>
                    Couldn´t load users
                </div> 
            )
            
        }
    }

    render(){
        return(
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-3">
                            <div className="row justify-content-center  header d-flex align-items-center">
                                <h2 className="title">Who are you?</h2>
                            </div>
                            <div className="row justify-content-center d-flex align-items-center distance">
                                {this.createUserModal()}
                            </div>
                            <div className="row justify-content-center">
                                <ul className="list-group  usersSize">
                                    {this.renderUsers()}
                                </ul>
                            </div>
                        </div>
                        <div className="col">
                            {(this.state.users.length===0)?null:<List ref="list"/>}
                        </div>                    
                    </div>
                </div>


                
            </div>
        )
    }
}

export default Home;