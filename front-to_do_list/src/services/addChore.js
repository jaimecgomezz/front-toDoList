import axios from 'axios';

export default (id, choreName) => {
    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    addChores(id:"${id}", data:{
                        chores:{
                            choreName:"${choreName}",
                            done: false,
                        }
                    }){
                        _id,
                        listName,
                        isActive,
                        chores{
                            choreName,
                            done
                        },
                        createdAt
                    }
                }
            `
        }  
    
    })
}