import axios from 'axios';

export default (id) => {

    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                query{
                    queryOneTaskList(id:"${id}"){
                        _id,
                        listName,
                        chores{
                            choreName,
                            done
                        }
                        isActive,
                        createdAt
                    }
                }
            `
        }  
    
    });
}