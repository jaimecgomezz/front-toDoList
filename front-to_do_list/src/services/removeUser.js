import axios from 'axios';

export default (id) => {

    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    removeU(id:"${id}"){
                        _id,
                        name,
                        isActive,
                        taskLists,
                        createdAt
                    }
                }
            `
        }  
    
    });
}