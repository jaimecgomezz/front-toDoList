import axios from 'axios';

export default (name) => {
    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    addU(data:{
                        name:"${name}",
                    }){
                        _id,
                        name,
                        isActive,
                        taskLists,
                        createdAt
                    }
                }
            `
        }  
    
    });
}