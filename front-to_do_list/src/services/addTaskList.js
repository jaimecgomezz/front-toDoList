import axios from 'axios';

export default (ownId,taskListName) => {
    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    addTL(data:{
                        listName:"${taskListName}",
                        userId:"${ownId}"
                    }){
                        _id,
                        listName,
                        isActive,
                        chores{
                            choreName,
                            done
                        }
                        createdAt
                    }
                }
            `
        }  
    
    })
}