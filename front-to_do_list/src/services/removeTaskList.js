import axios from 'axios';

export default (id, userId) => {
    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    removeTL(id:"${id}", data:{
                        userId: "${userId}"
                    }){
                        _id,
                        listName,
                        isActive,
                        chores{
                            choreName,
                            done
                        },
                        createdAt
                    }
                }
            `
        }  
    
    });
}