import axios from 'axios';

export default (id, choreName) => {
    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                mutation{
                    deleteChore(id:"${id}", choreName:"${choreName}"){
                        _id,
                        listName,
                        isActive,
                        chores{
                            choreName,
                            done
                        },
                        createdAt
                    }
                }
            `
        }  
    
    })
}