import axios from 'axios';

export default () => {

    return axios({
        url:'http://localhost:5000/graphql',
        method:'post',
        data:{
            query:`
                query{
                    queryAllU{
                        _id,
                        name,
                        isActive,
                        taskLists,
                        createdAt
                    }
                }
            `
        }  
    
    });
}