# To-do-list front-end

Este repositorio contiene los componentes que se muestran al cliente, diseñado con bootstrap. Cuando se hace alguna modificación
a la interfaz, se ve reflejada inmediatamente, con la ayuda de axios, realiza peticiones al servidor para traer la data necesaria.

## Primeros pasos

Para comenzar dembemos clonar la carpeta, ya sea desde este repositorio o mediante un fork al propio.

### Requisitos

Es necesario tener instalado un manejador de paquetes, ya sea Yarn, NPM, Bower u otros. 
En caso de usar un sistema operativo basado en Linux podemos acceder a la terminal, situarnos en la ruta donde deseemos 
clonar la carpeta y ejecutar el comando 
`git clone https://github.com/JaimeCGomez/front-toDoList.git`.

En caso de usar Windows, habrá que descargar git, acceder a su terminal -git bash- y correr el mismo comando.

### Instalación

Con la carpeta clonada procedemos a abrir la ruta en terminal, dentro hay una carpeta con el nombre `front-to_do_list`, 
accedemos a ella en terminal al nivel donde se encuentra el package.json.

Para este ejemplo, se utiliza npm. 
El comando es el siguiente:

1.Una vez dentro, ejecutamos el comando:
``` 
$ npm i
```

2.Finalizada la instalación procedemos a correr el siguiente comando:
``` 
$ npm start
```

A continuación los scripts de react compilaran los archivos y te redireccionarán a la siguiente pestaña en el navegador:
``` 
http://localhost:3000
```

Una vez ahí, está listo para interactuar con la interfaz, realizar cambios, lecturas y eleminar elementos.
Cada vez que se ejecuta un cambio en la interfaz, este realiza una peticion al servidor mediante axios, mandando un query
o un mutation según sea el caso, al recibir respuesta el componente se refresca para mostrar los datos actualizados.


## Creado con

* [ReactJS](https://reactjs.org/) - Libreria de JS para crear interfaz de usuario.
* [Bootstrap](https://getbootstrap.com/) - Kit de desarrollo para HTML, CSS y JS.
* [Axios](https://github.com/axios/axios) - Cliente de HTTP, basado en promesas, para NodeJS.

## Autores

* **Jaime Iván Castro Gómez** 



